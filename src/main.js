import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import { MediaQueries } from 'vue-media-queries'

import App from './App.vue'
import router from './router'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const mediaQueries = new MediaQueries()

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(mediaQueries)

new Vue({
  router,
  mediaQueries,
  render: h => h(App)
}).$mount('#app')
