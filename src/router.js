import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Questions from './views/Questions.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/team',
      name: 'team',
      component: Home
    },
    {
      path: '/record',
      name: 'record',
      component: Home
    },
    {
      path: '/questions',
      name: 'questions',
      component: Questions
    }
  ]
})
